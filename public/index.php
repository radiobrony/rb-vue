<?php
if (strpos($_SERVER['HTTP_USER_AGENT'], 'splash') !== false) {
  // Go to hell
  http_response_code(444);
  exit();
}

// Require composer autoloader
require __DIR__ . '/../vendor/autoload.php';

// Create DB instance
use Medoo\Medoo;

// Initialize DB
require __DIR__ . '/../dbconfig.php';
$db = new Medoo($dbconfig);

// User agents containing one of those string will get "player" type and 
// a simple cover as the image in the meta tags
$uaPlayer = ['twitter', 'mastodon', 'misskey', 'foundkey', 'pleroma', 'akkoma'];

// Helper function
function striposa(string $haystack, array $needles, int $offset = 0): bool {
  // Based on https://stackoverflow.com/a/9220624
  foreach ($needles as $needle) {
    if (stripos($haystack, $needle, $offset) !== false) {
      return true; // stop on first true result
    }
  }

  return false;
}

// Make a good <head> available without JS
function putHeader($title=null, $description=null, $author=null, $image=null, $player=null, $rss=null) {
  $out = file_get_contents(__DIR__ . '/index.html');

  if (!$title) $title = 'Radio Brony';
  if (!$description) $description = 'La webradio des Bronies, 20% plus baguette';
  if (!$author) $author = 'Radio Brony';

  if ($player && striposa($_SERVER['HTTP_USER_AGENT'], $uaPlayer)) {
    // If we have a player and it's the Twitter or Mastodon UA
    $cardType = 'player';
  } else {
    $cardType = $image ? 'summary_large_image' : 'summary';
  }
  $ogType = $player ? 'article' : 'website';

  if (!$image) $image = 'https://radiobrony.fr/img/icons/bwavatar-details.png';
  $imageHTTP = str_replace('https:', 'http:', $image);

  $canonical = 'https://' . $_SERVER['HTTP_HOST'] . strtok($_SERVER['REQUEST_URI'], '?');

  $h = <<<EOL
  <title>${title}</title>
  <meta name="author" content="${author}">
  <meta name="description" content="${description}">
  <link rel="canonical" href="${canonical}"/>
  <meta name="twitter:card" content="${cardType}">
  <meta name="twitter:title" content="${title}">
  <meta name="twitter:description" content="${description}">
  <meta name="twitter:image" content="${image}">
  <meta name="twitter:image:alt" content="${title}">
  <meta property="og:type" content="${ogType}">
  <meta property="og:title" content="${title}">
  <meta property="og:description" content="${description}">
  <meta property="og:url" content="https://radiobrony.fr${_SERVER['REQUEST_URI']}">
  <meta property="og:image" content="${imageHTTP}">
  <meta property="og:image:secure_url" content="${image}">

EOL;

  if ($rss) $h .= <<<EOF
  <link type="application/rss+xml" rel="alternate" title="${rss['title']}" href="${rss['link']}"/>

EOF;

  if ($player) {
    if (striposa($_SERVER['HTTP_USER_AGENT'], $uaPlayer)) {
      // Only for the Twitter or Mastodon UA
      $h .= <<<EOF
      <meta name="twitter:player" content="${player['url']}">
      <meta name="twitter:player:height" content="480">
      <meta name="twitter:player:width" content="852">
EOF;
    }
    $h .= <<<EOF
  <meta property="og:audio" content="${player['file']}">
  <meta property="article:published_time" content="${player['pub']}">
  <meta property="article:author" content="${author}">

EOF;
  }

  $out = str_replace('  <meta name="meta" content="hell">', $h, $out);

  // For noscript
  global $db;
  $nojs = '';

  $db_sA = $db->select('pod_shows', [
    'slug',
    'title'
  ], []);

  foreach($db_sA as $db_s) {
    $nojs .= <<<EOF

          <li><a href="https://pod.radiobrony.fr/feed/${db_s['slug']}">${db_s['title']}</a></li>
EOF;
  }

  $out = str_replace('<span class="more-feeds"></span>', $nojs, $out);

  // Return our page
  echo $out;
}

// Create Router instance
$router = new \Bramus\Router\Router();

// Define routes
$router->set404(function() {
  header('HTTP/1.1 404 Not Found');
  putHeader('Page non trouvée ? | Radio Brony');
});

$router->all('/', function() {
  putHeader('Radio Brony');
});

$router->all('/live', function() {
  putHeader('Live | Radio Brony');
});

$router->all('/faq', function() {
  putHeader('Questions fréquement posées (FAQ) | Radio Brony');
});

$router->all('/privacy', function() {
  putHeader('Politique de confidentialité | Radio Brony');
});

$router->all('/equipe', function() {
  putHeader('Équipe | Radio Brony');
});

$router->mount('/podcasts', function() use ($router) {
  $router->all('/', function() {
    putHeader('Podcasts | Radio Brony');
  });

  $router->mount('/show', function() use ($router) {
    $router->all('/([a-z0-9_-]+)', function($show) {
      global $db;
      $db_s = $db->get('pod_shows', [
        'title',
        'slug',
        'description',
        'author'
      ], [
        'slug' => $show
      ]);

      $image = 'https://pod.radiobrony.fr/og/show/' . $db_s['slug'];

      putHeader($db_s['title'] . ' | Podcasts | Radio Brony',
                $db_s['description'],
                $db_s['author'],
                $image,
                null,
                array(
                  'title' => $db_s['title'],
                  'link' => 'https://pod.radiobrony.fr/feed/' . $show
                ));
    });

    $router->all('/([a-z0-9_-]+)/episode/([\d.]+)/([\d.]+)', function($show, $s, $e) {
      global $db;

      $db_s = $db->get('pod_shows', [
        'title',
        'description',
        'author',
        'baseFolder'
      ], [
        'slug' => $show
      ]);

      $db_e = $db->get('pod_episodes', [
        'id',
        'title',
        'subtitle',
        'author',
        'image',
        'file',
        'pubDate'
      ], [
        'showSlug' => $show,
        'season' => $s,
        'episode' => $e
      ]);

      $description = !empty($db_e['subtitle']) ? $db_e['subtitle'] : $db_s['description'];
      $author = !empty($db_e['author']) ? $db_e['author'] : $db_s['author'];

      if (striposa($_SERVER['HTTP_USER_AGENT'], $uaPlayer)) {
        // Only for the Twitter or Mastodon UA
        $image = !empty($db_e['image']) ? 'https://pod.radiobrony.fr/img/covers/' . $db_e['image'] : 'https://pod.radiobrony.fr/img/shows/' . $show . '.png';
      } else {
        $image = 'https://pod.radiobrony.fr/og/episode/' . $db_e['id'];
      }

      putHeader($db_e['title'] . ' | ' . $db_s['title'] . ' | Radio Brony',
                $description, $author, $image,
                array(
                  'url' => 'https://pod.radiobrony.fr/player/' . $db_e['id'],
                  'file' => 'https://pod.radiobrony.fr/files/' . $db_s['baseFolder'] . $db_e['file'],
                  'pub' => date("c", strtotime($db_e['pubDate']))
                ),
                array(
                  'title' => $db_s['title'],
                  'link' => 'https://pod.radiobrony.fr/feed/' . $show
                ));
    });
  });
});

$router->mount('/page', function() use ($router) {
  $router->all('/2042', function() {
    putHeader('2042 | Radio Brony');
  });

  $router->all('/bandcamp-friday', function() {
    putHeader('Une sélection pour le Bandcamp Friday | Radio Brony');
  });
});

$router->all('/p', function() {
  header('Location: https://embed.radiobrony.fr');
});

$router->all('/friendship-is-magic-ce-que-pourraient-apprendre-de-mlp-les-artistes-independants', function() {
  putHeader('« Friendship is Magic » : ce que pourraient apprendre de MLP les artistes indépendants. | Radio Brony',
            'L\'article suivant est issu du site Pigeonsandplanes.com, dédié à toutes les scènes musicales : de la plus conventionnelle à la plus indépendante, tous genres confondus.',
            'Mijka');
});

$router->all('/dossier-comment-et-ou-trouver-de-la-musique-brony-aujourdhui', function() {
  putHeader('[Dossier] Comment et où trouver de la musique brony aujourd\'hui ? | Radio Brony',
            'Cet article part d’une question (posée par Butterscotch) lors de la 3PS : « Avant j\'arrivais à trouver de la musique brony sans trop de soucis en suivant quelques artistes, mais maintenant qu\'ils sortent moins de musique, c\'est plus difficile... Tu fais comment ? »',
            'Mijka');
});

// Run it!
$router->run();
