const pages = [
  {
    path: '/page/2042',
    name: 'page-2042',
    component: _ => import('@/views/Page/Page2042.vue')
  },
  {
    path: '/page/bandcamp-friday',
    name: 'page-brandcamp-friday',
    component: _ => import('@/views/Page/BandcampFriday.vue')
  }
]

export default pages
