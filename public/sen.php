<?php
$host = "sentry.io";
$known_project_ids = array( 1192223 );

$envelope = stream_get_contents(STDIN);
$pieces = explode("\n", $envelope, 2);
$header = json_decode($pieces[0], true);

if (isset($header["dsn"])) {
  $dsn = parse_url($header["dsn"]);
  $project_id = intval(trim($dsn["path"], "/"));

  if (in_array($project_id, $known_project_ids)) {
    $options = array(
      'http' => array(
        'header'  => "Content-type: application/x-sentry-envelope\r\n",
        'method'  => 'POST',
        'content' => $envelope
      )
    );

    echo file_get_contents(
      "https://$host/api/$project_id/envelope/",
      false,
      stream_context_create($options)
    );
  }
}