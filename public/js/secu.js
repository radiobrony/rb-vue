// Prevent Meta's and TikTok's apps from tracking some user interactions
// https://krausefx.com/blog/announcing-inappbrowsercom-see-what-javascript-commands-get-executed-in-an-in-app-browser
let alertMetaListener = false
let alertLCPListener = false

const originalEventListener = document.addEventListener
document.addEventListener = function (_, b) {
  // Instagram + FB: selectionchange (click)
  if (b.toString().indexOf('messageHandlers.fb_getSelection') > -1) {
    if (!alertMetaListener) {
      alertMetaListener = true // Trigger the alert once
      window.alert('Nous avons détecté que votre navigation est compromise. ' +
                   'L\'application que vous utilisez (ou une extension du navigateur) ' +
                   'est actuellement en train de vous espionner. Il peut voir ce que vous ' +
                   'cliquez et voir le texte que vous sélectionnez.')
    }

    return null
  }

  // TikTok: keydown, click
  if (b.toString().indexOf('LCPMonitor') > -1) {
    if (!alertLCPListener) {
      alertLCPListener = true // Trigger the alert once
      window.alert('Nous avons détecté que votre navigation est compromise. ' +
                   'L\'application que vous utilisez (ou une extension du navigateur) ' +
                   'est actuellement en train de vous espionner. Il peut voir ce que vous ' +
                   'cliquez et lire tout ce que vous écrivez, y compris les mots de passe.')
    }

    return null
  }

  return originalEventListener.apply(this, arguments)
}

// Detect BetterJsPop
setTimeout(function () {
  if (('BetterJsPop' in window || 'BetterJsPop' in document)) {
    window.alert('Nous avons détecté que votre navigation est compromise. ' +
                 'Une extension est présente pour vous espionner et intégrer ' +
                 'des publicités. Nous vous recommendons de repasser en revue ' +
                 'les extensions installés dans votre navigateur.')
  }
}, 5000)
