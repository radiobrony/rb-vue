/* eslint-disable no-useless-escape */
import { defineConfig } from 'vite'
import path from 'path'
import vue from '@vitejs/plugin-vue'
import envCompatible from 'vite-plugin-env-compatible'
import { EsLinter, linterPlugin } from 'vite-plugin-linter'
import { createHtmlPlugin } from 'vite-plugin-html'
import { viteCommonjs } from '@originjs/vite-plugin-commonjs'
import { VitePWA } from 'vite-plugin-pwa'
import { visualizer } from 'rollup-plugin-visualizer'
import viteSentry from 'vite-plugin-sentry'

import { GitRevisionPlugin } from 'git-revision-webpack-plugin'
const gitRevisionPlugin = new GitRevisionPlugin()

const sentryConfig = {
  dryRun: process.env.NODE_ENV === 'development', // Disable in DEV
  url: 'https://sentry.io',
  authToken: process.env.SENTRY_TOKEN,
  org: 'radiobrony',
  project: 'website',
  release: gitRevisionPlugin.version(),
  deploy: {
    env: (
      process.env.NODE_ENV === 'production' && process.env.CI_COMMIT_BRANCH === 'master'
        ? 'production'
        : 'development'
    )
  },
  setCommits: {
    auto: true
  },
  sourceMaps: {
    include: ['./dist/assets'],
    ignore: ['node_modules'],
    urlPrefix: '~/assets'
  }
}

// https://vitejs.dev/config/
export default defineConfig((configEnv) => ({
  resolve: {
    alias: [
      {
        find: '@',
        replacement: path.resolve(__dirname, 'src')
      }
      /*
      {
        find: /^leaflet/,
        replacement: path.resolve(__dirname, 'node_modules/leaflet')
      }
      */
    ],
    extensions: [
      '.mjs',
      '.js',
      '.ts',
      '.jsx',
      '.tsx',
      '.json',
      '.vue'
    ]
  },
  plugins: [
    linterPlugin({
      include: [
        './src/**/*.js',
        './src/**/*.vue'
      ],
      linters: [
        new EsLinter({
          configEnv,
          serveOptions: { clearCacheOnStart: true }
        })
      ]
    }),
    vue(),
    viteCommonjs(),
    envCompatible(),
    createHtmlPlugin({
      minify: false
    }),
    VitePWA({
      selfDestroying: true, // Start to unregister the SW
      filename: 'service-worker.js',
      workbox: {
        globPatterns: [
          '**\/*.{js,css,html,webmanifest}',
          'assets\/*.jpg',
          'browserconfig.xml',
          'img\/icons/*',
          'img\/logos/RBLogo.png',
          'js\/micromodal.min.js',
          'js\/secu.js',
          'js\/twitter.js',
          'radiobrony.m3u',
          'robots.txt'
        ]
      },
      manifest: {
        name: 'Radio Brony',
        short_name: 'RBrony',
        description: 'La webradio des Bronies, 20% plus baguette',
        icons: [
          {
            src: '/img/icons/android-chrome-36x36.png',
            sizes: '36x36',
            type: 'image/png'
          },
          {
            src: '/img/icons/android-chrome-48x48.png',
            sizes: '48x48',
            type: 'image/png'
          },
          {
            src: '/img/icons/android-chrome-72x72.png',
            sizes: '72x72',
            type: 'image/png'
          },
          {
            src: '/img/icons/android-chrome-96x96.png',
            sizes: '96x96',
            type: 'image/png'
          },
          {
            src: '/img/icons/android-chrome-144x144.png',
            sizes: '144x144',
            type: 'image/png'
          },
          {
            src: '/img/icons/android-chrome-192x192.png',
            sizes: '192x192',
            type: 'image/png'
          },
          {
            src: '/img/icons/android-chrome-256x256.png',
            sizes: '256x256',
            type: 'image/png'
          },
          {
            src: '/img/icons/android-chrome-384x384.png',
            sizes: '384x384',
            type: 'image/png'
          },
          {
            src: '/img/icons/android-chrome-512x512.png',
            sizes: '512x512',
            type: 'image/png'
          },
          {
            src: '/img/icons/maskable_icon-512x512.png',
            sizes: '512x512',
            type: 'image/png',
            purpose: 'maskable'
          }
        ],
        theme_color: '#73a2ab',
        background_color: '#73a2ab',
        start_url: '/',
        display: 'standalone',
        dir: 'ltr',
        lang: 'fr-FR',
        categories: [
          'music',
          'news',
          'entertainment'
        ],
        shortcuts: [
          {
            name: 'Live',
            description: 'Page actualisée en direct lors de nos émissions',
            url: '/live',
            icons: [
              {
                src: '/img/icons/android-chrome-96x96.png',
                sizes: '96x96',
                type: 'image/png'
              }
            ]
          },
          {
            name: 'Podcasts',
            description: 'La liste de nos émissions à (ré)écouter',
            url: '/podcasts',
            icons: [
              {
                src: '/img/icons/android-chrome-96x96.png',
                sizes: '96x96',
                type: 'image/png'
              }
            ]
          }
        ],
        screenshots: [
          {
            src: '/img/screenshots/1.png',
            type: 'image/png',
            sizes: '828x1792',
            platform: 'narrow',
            label: 'Accueil, thème sombre'
          },
          {
            src: '/img/screenshots/2.png',
            type: 'image/png',
            sizes: '828x1792',
            platform: 'narrow',
            label: 'Accueil, thème clair'
          },
          {
            src: '/img/screenshots/3.png',
            type: 'image/png',
            sizes: '828x1792',
            platform: 'narrow',
            label: 'Liste d\'épisodes d\'un podcast, thème clair'
          },
          {
            src: '/img/screenshots/4.png',
            type: 'image/png',
            sizes: '828x1792',
            platform: 'narrow',
            label: 'Page d\'un podcast, thème clair'
          },
          {
            src: '/img/screenshots/5.png',
            type: 'image/png',
            sizes: '2560x1440',
            platform: 'wide',
            label: 'Accueil, thème clair et large'
          },
          {
            src: '/img/screenshots/6.png',
            type: 'image/png',
            sizes: '2560x1440',
            platform: 'wide',
            label: 'Page d\'un podcast, thème sombre et large'
          }
        ]
      }
    }),
    viteSentry(sentryConfig),
    visualizer()
  ],
  build: {
    sourcemap: true,
    rollupOptions: {
      output: {
        manualChunks: {
          videojs: ['video.js']
          // leaflet: ['leaflet']
        }
      }
    }
  }
}))
