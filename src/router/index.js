/* global Shynet */
import { createRouter, createWebHistory } from 'vue-router'
import pages from './pages'
import oldblog from './oldblog'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: _ => import('@/views/Home.vue')
    },
    {
      path: '/live',
      name: 'live',
      component: _ => import('@/views/Live.vue')
    },
    {
      path: '/faq',
      name: 'faq',
      component: _ => import('@/views/FAQ.vue')
    },
    {
      path: '/privacy',
      name: 'privacy',
      component: _ => import('@/views/Privacy.vue')
    },
    {
      path: '/equipe',
      name: 'equipe',
      component: _ => import('@/views/Equipe.vue')
    },
    {
      path: '/category/podcasts',
      redirect: {
        name: 'podcast-list'
      },
      children: [
        {
          path: ':show',
          redirect: to => {
            return {
              name: 'podcast-show',
              params: {
                show: to.params.show.replace('-', '')
              }
            }
          }
        }
      ]
    },
    {
      path: '/podcasts',
      component: _ => import('@/views/Podcast/Podcast.vue'),
      children: [
        {
          path: '',
          name: 'podcast-list',
          component: _ => import('@/views/Podcast/PodcastList.vue')
        },
        {
          path: 'show/:show',
          name: 'podcast-show',
          component: _ => import('@/views/Podcast/PodcastShow.vue')
        },
        {
          path: 'show/:show/episode/:se/:ep',
          name: 'podcast-episode',
          component: _ => import('@/views/Podcast/PodcastEpisode.vue')
        }
      ]
    },
    {
      path: '/p',
      name: 'popup',
      beforeEnter: _ => {
        window.location = 'https://embed.radiobrony.fr/'
      }
    },
    ...pages,
    ...oldblog,
    {
      path: '/:pathMatch(.*)*',
      name: 'notfound',
      component: _ => import('@/views/NotFound.vue')
    }
  ],
  // This will make sure we come back to the top on page change
  scrollBehavior (to, from, savedPosition) {
    if (to.path !== from.path) {
      // Only act if path changed
      if (savedPosition) {
        // Delay by 150ms due to transition getting in the way
        // Will not work if we have to load stuff but oh well
        return new Promise((resolve, reject) => {
          setTimeout(_ => {
            resolve(savedPosition)
          }, 150)
        })
      } else if (to.hash && to.hash.substring(0, 2) !== '#t') {
        // Smooth scroll to anchor, if it isn't the timeskip hash
        return {
          el: to.hash,
          behavior: 'smooth'
        }
      } else {
        return {
          top: 0
        }
      }
    }
  }
})

router.afterEach((to, from, failure) => {
  if (!failure && to.path !== from.path && 'Shynet' in window) {
    // Notify Shynet about a new page load
    Shynet.newPageLoad()
  }
})

export default router
