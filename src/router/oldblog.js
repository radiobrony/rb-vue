/* Ici on récupère quelques pages du vieu site par soucis de préservation */
const oldblog = [
  {
    path: '/friendship-is-magic-ce-que-pourraient-apprendre-de-mlp-les-artistes-independants',
    name: 'oldblog-fim-artistes-indes',
    component: _ => import('@/views/OldBlog/fim-artistes-indes.vue')
  },
  {
    path: '/dossier-comment-et-ou-trouver-de-la-musique-brony-aujourdhui',
    name: 'oldblog-dossier-trouver-sauce',
    component: _ => import('@/views/OldBlog/dossier-trouver-sauce.vue')
  }
]

export default oldblog
