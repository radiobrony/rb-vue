<?php
// CORS friendly
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');

// Our Icecast JSON endpoint
$iceUrl = 'http://localhost:8000/status-json.xsl';

// Count
$count = intval(file_get_contents('/var/www/radiohls-count'), 10);

function fetch($url) {
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_HEADER, false);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_TIMEOUT, 10);
  $output = curl_exec($ch);
  curl_close($ch);
  $output = str_replace('http://Acheron:8000', 'https://radiobrony.live', $output);
  return $output;
}

$j = json_decode(fetch($iceUrl));

$o = array(
  'now_playing' => [],
  'listeners' => $count,
  'sources' => []
);

foreach($j->icestats->source as $s) {
  $o['listeners'] += $s->listeners;
  $t = substr(strrchr($s->listenurl, '/'), 1);
  $o['sources'][$t] = array(
    'title' => $s->server_name,
    'url' => $s->listenurl,
    'mime' => $s->server_type,
    'audio_info' => $s->audio_info
  );

  if ($t == 'aac') {
    $split = explode(' - ', $s->title, 2);
    $o['now_playing'] = array(
        'full' => $s->title,
        'artist' => $split[0],
        'title' => $split[1]
    );
  }
}

$o['sources']['hls'] = array(
  'title' => 'Radio Brony (HLS)',
  'url' => 'https://radiobrony.live/hls/live.m3u8',
  'mime' => 'application/vnd.apple.mpegurl',
  'audio_info' => 'channels=2;samplerate=44100'
);

echo json_encode($o);
