<?php
// CORS friendly
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');

// Our Icecast JSON endpoint
$iceUrl = 'http://localhost:8000/status-json.xsl';

// Config stations (priority based on order !)
$mp3s = array('/mp3', '/live');
$oggs = array('/ogg', '/live.ogg');
$aacs = array('/aac');

function addHLS($r) {
	$j = json_decode($r, true);
	$i = -1;

	if (array_key_exists("listeners", $j["icestats"]["source"])) {
		if ($j["icestats"]["source"]["listenurl"] == "https://radiobrony.live/aac") {
			$count = file_get_contents('/var/www/radiohls-count');
			$j["icestats"]["source"]["listeners"] += intval($count);
			$j["icestats"]["source"]["listeners_fromhls"] = intval($count);
		}
	} else {
		foreach ($j["icestats"]["source"] as $k => $out) {
			if ($out["listenurl"] == "https://radiobrony.live/aac") $i = $k;
		}

		if ($i >= 0) {
			$count = file_get_contents('/var/www/radiohls-count');
			$j["icestats"]["source"][$i]["listeners"] += intval($count);
			$j["icestats"]["source"][$i]["listeners_fromhls"] = intval($count);
		}
	}

	return json_encode($j);
}

function fetch($url) {
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	$output = curl_exec($ch);
	curl_close($ch);
	$output = str_replace('http://Acheron:8000', 'https://radiobrony.live', $output);
	return $output;
}

if (!empty($_GET['current'])) {
	// Get the available endpoint for either mp3 or ogg
	$stations = array();

	if ($_GET['current'] == 'mp3') {
		$stations = $mp3s;
	} else if ($_GET['current'] == 'ogg') {
		$stations = $oggs;
	} else if ($_GET['current'] == 'aac') {
		$stations = $aacs;
	}

	foreach ($stations as $station) {
		$response = fetch($iceUrl . '?mount=' . $station);
		$json = json_decode($response, true);

		// If stream_start is set, it means the mount is active
		if (isset($json['icestats']['source']['stream_start'])) {
			echo addHLS($response);
			break;
		}
	}
} else if (!empty($_GET['mount'])) {
	// Get specific mount
	echo addHLS(fetch($iceUrl . '?mount=' . $_GET['mount']));
} else {
	// Just proxy from Icecast
	echo addHLS(fetch($iceUrl));
}
