/* eslint-disable no-console */
import { registerSW } from 'virtual:pwa-register'

const updateSW = registerSW({
  onNeedRefresh () {
    console.log('Une mise à jour est disponible.')
    window.MicroModal.show('updatingModal')
    setTimeout(updateSW, 1000)
  },
  onOfflineReady () {
    console.log('L\'application est désormais en cache.')
  },
  onRegisterError (error) {
    console.error('Erreur lors de l\'enregistrement du service worker:', error)
  }
})
