# Vue Website
> Site web de [Radio Brony](https://radiobrony.fr/)

```
# Deps
yarn

# Dev
yarn serv

# Config Prod
composer install
cp dbconfig.example.php dbconfig.php
$EDITOR dbconfig.php

# Build
yarn build
```

Pointez le serveur web vers le dossier `dist/`.
