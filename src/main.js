// Vue stuff
import { createApp } from 'vue'
import App from './App.vue'
import router from './router/index'
import store from './store'

// Modules
import 'virtual:vite-plugin-sentry/sentry-config'
import VueNativeSock from 'vue-native-websocket'
import * as Sentry from '@sentry/vue'
import { Integrations } from '@sentry/tracing'
import 'proxy-polyfill/src/proxy'
import 'promise-polyfill/src/polyfill'
import 'whatwg-fetch'
import './registerServiceWorker'

// CSS
import 'normalize.css'
import '@mkody/vue-material-design-icons/styles.css'
import 'vue2-animate/dist/vue2-animate.min.css'

const dist = import.meta.env.VITE_PLUGIN_SENTRY_CONFIG.dist
const release = import.meta.env.VITE_PLUGIN_SENTRY_CONFIG.release

// Say hi to whoever opens the console
console.log(
  '%cRADIO\u00A0\u00A0\n%c     BRONY\u00A0\u00A0',
  'margin-bottom:-15px;color:#a3d7ee;-webkit-text-stroke:1px #79b3d1;text-stroke:1px #79b3d1;font-weight:bold;font-size:50px;font-family:"GROBOLD","Helvetica Neue","Helvetica","Arial",sans-serif;',
  'color:#cb313c;-webkit-text-stroke:1px #a71721;text-stroke:1px #a71721;font-weight:bold;font-size:50px;font-family:"GROBOLD","Helvetica Neue","Helvetica","Arial",sans-serif;'
)
console.log(
  '%cGit: https://gitlab.com/radiobrony/rb-vue' +
  '\nVersion: ' + release +
  '\nEnv: ' + import.meta.env.MODE,
  'color:green;'
)

// Make our app
const rbApp = createApp(App)

// Plug the basic stuff
rbApp.use(router)
rbApp.use(store)

// If we're on an appropariate domain, enable Sentry
if (window.location.hostname.indexOf('radiobrony.') !== -1) {
  Sentry.init({
    rbApp,
    dsn: 'https://f690f4239bc84454ac5a04ce3785a36c@o146961.ingest.sentry.io/1192223',
    environment: import.meta.env.MODE,
    integrations: [
      new Integrations.BrowserTracing({
        routingInstrumentation: Sentry.vueRouterInstrumentation(router),
        tracingOrigins: [/radiobrony\./, /^\//]
      })
    ],
    dist,
    release,
    logErrors: true,
    tracesSampleRate: 1.0,
    tunnel: 'https://node.radiobrony.fr/sen.php'
  })
}

// Websockets
rbApp.use(
  VueNativeSock,
  'wss://node.radiobrony.fr',
  {
    format: 'json',
    reconnection: true,
    reconnectionDelay: 10000, // 10 seconds
    store
  }
)

// LET'S GOOOOOOO
rbApp.mount('#rbApp')
